package main

import (
	"database/sql"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"regexp"

	_ "github.com/go-sql-driver/mysql"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"

	jwtmiddleware "github.com/auth0/go-jwt-middleware"
	"github.com/dgrijalva/jwt-go"
)

var mySecret = []byte("nekinekineki")

type User struct {
	DbID     string
	Username string
	Password string
	Type     string
	Token    string
}

type Exam struct {
	Content string
}

var jwtMiddleware = jwtmiddleware.New(jwtmiddleware.Options{
	ValidationKeyGetter: func(token *jwt.Token) (interface{}, error) {
		return mySecret, nil
	},
	Extractor:     jwtmiddleware.FromFirst(jwtmiddleware.FromAuthHeader),
	UserProperty:  "user",
	SigningMethod: jwt.SigningMethodHS256,
})

var isValid = regexp.MustCompile(`^[0-9a-zA-Z]+$`).MatchString

func getToken(writer http.ResponseWriter, r *http.Request) {
	writer.Header().Set("Content-Type", "application/json")
	var user User
	_ = json.NewDecoder(r.Body).Decode(&user)
	user.Username = decodeMyBase(user.Username)
	fmt.Println("REQUEST FOR:  U=" + user.Username + "    P=" + user.Password)
	if user.Username != "" && user.Password != "" {
		db, err := sql.Open("mysql", "root:rootpassword@tcp(mysql:3306)/tryMe")
		if err != nil {
			log.Fatal(err)
			panic(err.Error())
		}

		err = db.Ping()
		if err != nil {
			panic(err.Error())
		} else {
			fmt.Println("CONNECTED TO DATABASE!")
		}

		preperd, err := db.Prepare("select * from User where username=? and password=?")
		resault, err := preperd.Query(user.Username, user.Password)
		if err != nil {
			panic(err.Error())
		} else {
			for resault.Next() {
				var tmpuser User
				err = resault.Scan(&tmpuser.DbID, &tmpuser.Username, &tmpuser.Password, &tmpuser.Type)
				if err != nil {
					panic(err.Error())
				} else {
					fmt.Println("APPROVED!!!")
					token := jwt.New(jwt.SigningMethodHS256)
					claims := token.Claims.(jwt.MapClaims)
					claims["DbID"] = tmpuser.DbID
					claims["username"] = tmpuser.Username
					claims["password"] = tmpuser.Password
					claims["type"] = tmpuser.Type
					tokenString, _ := token.SignedString(mySecret)
					writer.Write([]byte("{ 'token' :'" + tokenString + "', 'type' : '" + tmpuser.Type + "' }"))
					defer db.Close()
					return
				}
			}
		}
	}
	fmt.Println("DENIED!!!")
	writer.WriteHeader(http.StatusUnauthorized)
}

//keycloak!!!

func decodeMyBase(encoded string) string {
	decoded, err := base64.StdEncoding.DecodeString(encoded)
	if err != nil {
		fmt.Println("decode error:", err)
		return "error not a base64"
	}
	return string(decoded)
	// Output:
	// "some data with \x00 and \ufeff"
}

func getCurrentContent(writer http.ResponseWriter, r *http.Request) {
	var USERID string

	//type of user
	user := r.Context().Value("user")
	for k, v := range user.(*jwt.Token).Claims.(jwt.MapClaims) {
		fmt.Fprintf(os.Stdout, "%s :\t%#v\n", k, v)
		if k == "type" {
			USERID = v.(string)
		}
	}

	fmt.Println("///GETTING content for ", USERID)
	writer.Header().Set("Content-Type", "text/plain")

	file, err := os.Open("/var/files/first.cpp")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	yourFile, err := ioutil.ReadAll(file)

	writer.Write([]byte(yourFile))

	return

}

func getExam(writer http.ResponseWriter, r *http.Request) {
	var USERID string

	//type of user
	user := r.Context().Value("user")
	for k, v := range user.(*jwt.Token).Claims.(jwt.MapClaims) {
		fmt.Fprintf(os.Stdout, "%s :\t%#v\n", k, v)
		if k == "type" {
			USERID = v.(string)
		}
	}

	fmt.Println("///GETTING content for ", USERID)
	writer.Header().Set("Content-Type", "text/plain")

	file, err := os.Open("/var/files/first.cpp")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	yourFile, err := ioutil.ReadAll(file)

	writer.Write([]byte(yourFile))

	return

}

func setExam(writer http.ResponseWriter, r *http.Request) {
	var USERID string

	//type of user
	user := r.Context().Value("user")
	for k, v := range user.(*jwt.Token).Claims.(jwt.MapClaims) {
		fmt.Fprintf(os.Stdout, "%s :\t%#v\n", k, v)
		if k == "type" {
			USERID = v.(string)
		}
	}

	fmt.Println("///GETTING content for ", USERID)

	if USERID == "admin" {
		decoder := json.NewDecoder(r.Body)

		var data Exam
		err := decoder.Decode(&data)
		if err != nil {
			panic(err)
		}

		fmt.Println("///WRITING TO FILE....", data.Content)

		//////////////////////////////////////////////////////////////////////
		f, err := os.Create("/var/files/first.cpp")
		if err != nil {
			log.Fatal(err)
		}
		l, err := f.WriteString(data.Content)
		if err != nil {
			fmt.Println(err)
			f.Close()
			return
		}
		fmt.Println(l, "bytes written successfully")
		err = f.Close()
		if err != nil {
			fmt.Println(err)
			return
		}
		//////////////////////////////////////////////////////////////////////

		writer.Header().Set("Content-Type", "text/plain")
		writer.Write([]byte("FILE WRITE SUCESSFULL"))
		return
	} else {
		fmt.Println("DENIED!!!")
		writer.WriteHeader(http.StatusUnauthorized)
	}

}

func main() {
	fmt.Println("[+]listening on port :8001")
	router := mux.NewRouter().StrictSlash(true)
	router.Use(func(h http.Handler) http.Handler { return handlers.LoggingHandler(os.Stdout, h) })
	router.Use(handlers.CORS())
	router.HandleFunc("/auth/getToken", getToken).Methods("POST")
	learnRouter := router.PathPrefix("/content").Subrouter()
	learnRouter.Use(jwtMiddleware.Handler)
	learnRouter.HandleFunc("/", getExam).Methods("GET")
	learnRouter.HandleFunc("/", setExam).Methods("POST")
	log.Fatal(http.ListenAndServe(":8001", router))
}
