<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>FILE SHARE</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <link href="css/login.css" rel="stylesheet">
  </head>

  <body>
    <div class="wrapper">
      <form class="form-signin" method="POST" action="phpScripts/preveri.php">
        <h2 class="form-signin-heading text-center">ONLINE LEARN <img src="https://i0.wp.com/www.theodrama.com/wp-content/uploads/2016/06/brain-icon-png-12.png?w=180" width="60" height="60"></h2>
        </br>
        <label for="inputEmail">Enter your username</label>
        <input type="text" class="form-control" name="user"  id="inputEmail" placeholder="Username" required="" autofocus="" />
        </br>
        <label for="inputPassword">Provide a password</label>
        <input type="password" class="form-control" name="pass" placeholder="Password" id="inputPassword" required=""/>
        </br>
        <button class="btn btn-lg btn-primary btn-block" type="submit" name="prijava">Login</button>
          <?php
          if(isset($_COOKIE["pazi"])) {
                 echo '<label>'.$_COOKIE['pazi'].'</label>';
          }
            ?>
      </form>
    </div>
  </body>
</html>
