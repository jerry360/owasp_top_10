<?php session_start();
require 'phpScripts/checkUser.php';
if(isset($_SESSION["idUser"]) && isset($_SESSION["username"])) {
    if(checkHim($_SESSION["idUser"], $_SESSION["username"])==false) {
        setcookie("pazi", "vsak poskus vdora bo prijavljen!");
        echo("<script>location.href = '../index.php';</script>");
        session_destroy();
    }else{
        if(isset($_GET['type'])) {
            if($_GET['type']=='admin') {
                echo("<script>location.href = '../dashboard.php?type=admin';</script>");
            }else if($_GET['type']=='user') {
                echo "status: wellcome ".$_SESSION["username"];
            }else{
                setcookie("pazi", "vsak poskus vdora bo prijavljen!");
                echo("<script>location.href = '../index.php';</script>");
                session_destroy();
            }
        }else{
            setcookie("pazi", "vsak poskus vdora bo prijavljen!");
            echo("<script>location.href = '../index.php';</script>");
            session_destroy();
        }

    }
}else{
    setcookie("pazi", "vsak poskus vdora bo prijavljen!");
    echo("<script>location.href = '../index.php';</script>");
    session_destroy();
}

?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <title>Hello, world!</title>
  </head>
  <div>
    <body>
  <nav class="navbar navbar-light bg-light">
    <a class="navbar-brand" href="#">
      <img src="https://upload.wikimedia.org/wikipedia/en/thumb/1/1c/OpenCL_Logo.svg/1024px-OpenCL_Logo.svg.png" width="50" height="50" class="d-inline-block align-top" alt="" >
        How to code in OPENCL paralel
    </a>
  </nav>


  <div class="container">
    <div class="row">
      <div class="col-md-9 offset-md-3">
        <h1>Today's lesson</h1>
        This sample implements bitonic sort algorithm for batches of short arrays
      </div>
    </div>
    <div id="open_file"></div>
  </div>
  </div>
  <div class="container">
  <div class="row top-buffer">
    <div class="col-md-9 offset-md-3" id="file_prog" >
    </div>
  </div>
  </div>

</body>

<script>

window.addEventListener("load", function(){
      get_file("first.cpp");
});




function get_file(data){

jQuery.ajax({
  type: "POST",
  url: './executables.php',
  data: {functionname: 'output_prog', type: [data]},
  success:function(data) {
  document.getElementById("file_prog").innerHTML=data;
  document.getElementById("file_prog").style.fontSize="small";
  }
});
}






</script>

</html>
