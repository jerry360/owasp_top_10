<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>FILE SHARE</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <link href="css/login.css" rel="stylesheet">
  </head>

  <body>
    <div class="wrapper">
      <form class="form-signin" method="POST" action="phpScripts/register.php">
        <h2 class="form-signin-heading text-center">REGISTER TO CLOUD FILE SHARE <img src="http://www.clker.com/cliparts/e/N/j/N/U/Q/thunder-cloud-s-cutie-mark-md.png" width="60" height="60"></h2>
        </br>
        <label for="inputUsername">Enter your username</label>
        <input type="text" class="form-control" name="user"  id="inputUsername" placeholder="Username" required="" autofocus="" />
        </br>
        <label for="inputEmail">Write your email</label>
        <input type="text" class="form-control" name="email" placeholder="Email" id="inputEmail" required=""/>
        </br>
        <label for="inputPassword">Provide a password</label>
        <input type="password" class="form-control" name="pass" placeholder="Password" id="inputPassword" required=""/>
        </br>
        <button class="btn btn-lg btn-primary btn-block" type="submit" name="register">Register</button>
        <div id="statMsg"></div>
      </form>
    </div>
  </body>
  <script>
    window.addEventListener("load", function(){
        document.getElementById("statMsg").innerHTML=getCookie("pazi");
      });

    function getCookie(cname) {
      var name = cname + "=";
      var decodedCookie = decodeURIComponent(document.cookie);
      var ca = decodedCookie.split(';');
      for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
          c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
          return c.substring(name.length, c.length);
        }
      }
      return "";
    }

  </script>
</html>
