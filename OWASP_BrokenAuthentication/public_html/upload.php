<?php session_start();

require 'phpScripts/checkUser.php';
if(isset($_SESSION["idUser"]) && isset($_SESSION["username"])) {
    if(checkHim($_SESSION["idUser"], $_SESSION["username"])==false) {
        setcookie("pazi", "vsak poskus vdora bo prijavljen!");
        echo("<script>location.href = '../index.php';</script>");
        session_destroy();
    }else{
        echo "status: wellcome ".$_SESSION["username"];
    }
}else{
    setcookie("pazi", "vsak poskus vdora bo prijavljen!");
    echo("<script>location.href = '../index.php';</script>");
    session_destroy();
}

?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>FILE SHARE</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link href="css/upload.css" rel="stylesheet">

  </head>

  <body>
      <form  method="post" enctype="multipart/form-data" id="data">
          <input class="uploads" type="file" name="fileToUpload" id="fileToUpload" onchange="send()"/>
          <h3><button class="uploads" id="dataButton">UPLOAD IT!</button></h3>
      </form>
      <h3><label class="status" id="stat">select an image file to continue</label></h3>
  </body>
</html>

<script>

function send(){
  var file=document.getElementById("fileToUpload");
  var txt = "";
if ('files' in file) {
  if (file.files.length == 0) {
    txt = "Select one or more files.";
  } else {
    if (file.files.length == 1) {
      var file = file.files[0];
      if ('name' in file) {
        txt += "name: " + file.name + "<br>";
      }
      if ('size' in file) {
        txt += "size: " + file.size + " bytes <br>";
      }
    }else{


      txt="select ONE file!!"
    }
  }
}
document.getElementById ("stat").innerHTML = txt;

}


$("form#data").submit(function(e) {

    var url = window.location.href; // or window.location.href for current url
    var captured = /user=([^&]+)/.exec(url)[1];
    var result = captured ? captured : 'myDefaultValue';



    e.preventDefault();
    var formData = new FormData(this);

    console.log(formData);
    jQuery.ajax({
      type: "POST",
      url: './phpScripts/file.php?user='+result,
      data: formData,
      success: function (data) {
        document.getElementById ("stat").innerHTML = data;
        console.log("server.log file updated");
        },
      cache: false,
      contentType: false,
      processData: false
    });
});


</script>
