-- MySQL Script generated by MySQL Workbench
-- Wed Aug  7 10:49:11 2019
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema programing
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `programing` ;

-- -----------------------------------------------------
-- Schema programing
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `programing` DEFAULT CHARACTER SET utf8 ;
USE `programing` ;

-- -----------------------------------------------------
-- Table `programing`.`tutorials`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `programing`.`tutorials` ;

CREATE TABLE IF NOT EXISTS `programing`.`tutorials` (
  `idtutorials` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  `content` LONGTEXT NOT NULL,
  PRIMARY KEY (`idtutorials`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `programing`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `programing`.`users` ;

CREATE TABLE IF NOT EXISTS `programing`.`users` (
  `idUsers` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(200) NOT NULL,
  `password` VARCHAR(200) NOT NULL,
  PRIMARY KEY (`idUsers`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `programing`.`tutorials`
-- -----------------------------------------------------
START TRANSACTION;
USE `programing`;
INSERT INTO `programing`.`tutorials` (`name`, `content`) VALUES ('opencl','%20%20%20%20%3Cbody%20onload%3D%22get_file%28%27first_opencl.cpp%27%29%22%3E%0A%20%20%3Cnav%20class%3D%22navbar%20navbar-light%20bg-light%22%3E%0A%20%20%20%20%3Ca%20class%3D%22navbar-brand%22%20href%3D%22%23%22%3E%0A%20%20%20%20%20%20%3Cimg%20src%3D%22https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fen%2Fthumb%2F1%2F1c%2FOpenCL_Logo.svg%2F1024px-OpenCL_Logo.svg.png%22%20width%3D%2250%22%20height%3D%2250%22%20class%3D%22d-inline-block%20align-top%22%20alt%3D%22%22%20%3E%0A%20%20%20%20%20%20%20%20How%20to%20code%20in%20OPENCL%20paralel%0A%20%20%20%20%3C%2Fa%3E%0A%20%20%20%20%3Cselect%20class%3D%22custom-select%22%20onchange%3D%27izbral_temo%28%29%27%20id%3D%22selection%22%3E%0A%20%20%20%20%20%20%3Coption%20value%3D%22opencl%22%3Elearn%20OpenCL%3C%2Foption%3E%0A%20%20%20%20%20%20%3Coption%20value%3D%22cuda%22%3Elearn%20CUDA%3C%2Foption%3E%0A%20%20%20%20%3C%2Fselect%3E%0A%0A%20%20%3C%2Fnav%3E%0A%0A%0A%20%20%3Cdiv%20class%3D%22container%22%3E%0A%20%20%20%20%3Cdiv%20class%3D%22row%22%3E%0A%20%20%20%20%20%20%3Cdiv%20class%3D%22col-md-9%20offset-md-3%22%3E%0A%20%20%20%20%20%20%20%20%3Ch1%3EGetting%20started%20with%20OpenCL%20and%20GPU%20Computing%3C%2Fh1%3E%0AOpenCL%20%28Open%20Computing%20Language%29%20is%20a%20new%20framework%20for%20writing%20programs%20that%20execute%20in%20parallel%20on%20different%20compute%20devices%20%28such%20as%20CPUs%20and%20GPUs%29%20from%20different%20vendors%20%28AMD%2C%20Intel%2C%20ATI%2C%20Nvidia%20etc.%29.%20The%20framework%20defines%20a%20language%20to%20write%20%E2%80%9Ckernels%E2%80%9D%20in.%20These%20kernels%20are%20the%20functions%20which%20are%20to%20run%20on%20the%20different%20compute%20devices.%20In%20this%20post%20I%20explain%20how%20to%20get%20started%20with%20OpenCL%20and%20how%20to%20make%20a%20small%20OpenCL%20program%20that%20will%20compute%20the%20sum%20of%20two%20lists%20in%20parallel.%0A%20%20%20%20%20%20%3C%2Fdiv%3E%0A%20%20%20%20%3C%2Fdiv%3E%0A%20%20%20%20%3Cdiv%20class%3D%22row%20top-buffer%22%3E%0A%20%20%20%20%20%20%3Cdiv%20class%3D%22col-md-9%20offset-md-3%22%3E%0A%20%20%20%20%20%20%20%20%3Ch2%3EYour%20first%20OpenCL%20program%20%E2%80%93%20Vector%20addition%3C%2Fh2%3E%0ATo%20demonstrate%20OpenCL%20I%20explain%20how%20to%20perform%20the%20simple%20task%20of%20vector%20addition.%20Suppose%20we%20have%20two%20lists%20of%20numbers%2C%20A%20and%20B%2C%20of%20equal%20size.%20The%20task%20of%20vector%20addition%20is%20to%20add%20the%20elements%20of%20A%20with%20the%20elements%20of%20B%20and%20put%20the%20result%20in%20the%20element%20of%20a%20new%20list%20called%20C%20of%20the%20same%20size.%20The%20figure%20below%20explains%20the%20operation.%0A%20%20%20%20%20%20%3C%2Fdiv%3E%0A%20%20%20%20%3C%2Fdiv%3E%0A%0A%20%20%3C%2Fdiv%3E%0A%3C%2Fbody%3E');
INSERT INTO `programing`.`tutorials` (`name`, `content`) VALUES ('cuda', '%20%20%20%20%3Cbody%20onload%3D%22get_file%28%27first.cpp%27%29%22%3E%0A%20%20%20%20%20%20%3Cnav%20class%3D%22navbar%20navbar-light%20bg-light%22%3E%0A%20%20%20%20%20%20%20%20%3Ca%20class%3D%22navbar-brand%22%20href%3D%22%23%22%3E%0A%20%20%20%20%20%20%20%20%20%20%3Cimg%20src%3D%22https%3A%2F%2Fdl2.macupdate.com%2Fimages%2Ficons256%2F27014.png%3Fd%3D1549389872%22%20width%3D%2250%22%20height%3D%2250%22%20class%3D%22d-inline-block%20align-top%22%20alt%3D%22%22%20%3E%0A%20%20%20%20%20%20%20%20%20%20%20%20How%20to%20code%20in%20CUDA%20paralel%0A%20%20%20%20%20%20%20%20%3C%2Fa%3E%0A%20%20%20%20%20%20%20%20%3Cselect%20class%3D%22custom-select%22%20onchange%3D%27izbral_temo%28%29%27%20id%3D%22selection%22%3E%0A%20%20%20%20%20%20%20%20%20%20%3Coption%20value%3D%22cuda%22%3Elearn%20CUDA%3C%2Foption%3E%0A%20%20%20%20%20%20%20%20%20%20%3Coption%20value%3D%22opencl%22%3Elearn%20OpenCL%3C%2Foption%3E%0A%20%20%20%20%20%20%20%20%3C%2Fselect%3E%0A%0A%20%20%20%20%20%20%3C%2Fnav%3E%0A%0A%0A%20%20%20%20%20%20%3Cdiv%20class%3D%22container%22%3E%0A%20%20%20%20%20%20%20%20%3Cdiv%20class%3D%22row%22%3E%0A%20%20%20%20%20%20%20%20%20%20%3Cdiv%20class%3D%22col-md-9%20offset-md-3%22%3E%0A%20%20%20%20%20%20%20%20%20%20%20%20%3Ch1%3ECUDA%20Programming%20Model%20Basics%3C%2Fh1%3E%0A%20%20%20%20%20%20%20%20%20%20%20%20Before%20we%20jump%20into%20CUDA%20C%20code%2C%20those%20new%20to%20CUDA%20will%20benefit%20from%20a%20basic%20description%20of%20the%20CUDA%20programming%20model%20and%20some%20of%20the%20terminology%20used.%0A%0A%20%20%20%20%20%20%20%20%20%20%20%20The%20CUDA%20programming%20model%20is%20a%20heterogeneous%20model%20in%20which%20both%20the%20CPU%20and%20GPU%20are%20used.%20In%20CUDA%2C%20the%20host%20refers%20to%20the%20CPU%20and%20its%20memory%2C%20while%20the%20device%20refers%20to%20the%20GPU%20and%20its%20memory.%20Code%20run%20on%20the%20host%20can%20manage%20memory%20on%20both%20the%20host%20and%20device%2C%20and%20also%20launches%20kernels%20which%20are%20functions%20executed%20on%20the%20device.%20These%20kernels%20are%20executed%20by%20many%20GPU%20threads%20in%20parallel.%0A%0A%20%20%20%20%20%20%20%20%20%20%20%20Given%20the%20heterogeneous%20nature%20of%20the%20CUDA%20programming%20model%2C%20a%20typical%20sequence%20of%20operations%20for%20a%20CUDA%20C%20program%20is%3A%0A%0A%20%20%20%20%20%20%20%20%20%20%20%20Declare%20and%20allocate%20host%20and%20device%20memory.%0A%20%20%20%20%20%20%20%20%20%20%20%20Initialize%20host%20data.%0A%20%20%20%20%20%20%20%20%20%20%20%20Transfer%20data%20from%20the%20host%20to%20the%20device.%0A%20%20%20%20%20%20%20%20%20%20%20%20Execute%20one%20or%20more%20kernels.%0A%20%20%20%20%20%20%20%20%20%20%20%20Transfer%20results%20from%20the%20device%20to%20the%20host.%0A%20%20%20%20%20%20%20%20%20%20%20%20Keeping%20this%20sequence%20of%20operations%20in%20mind%2C%20let%E2%80%99s%20look%20at%20a%20CUDA%20C%20example.%0A%20%20%20%20%20%20%20%20%20%20%3C%2Fdiv%3E%0A%20%20%20%20%20%20%20%20%3C%2Fdiv%3E%0A%20%20%20%20%20%20%20%20%3Cdiv%20class%3D%22row%20top-buffer%22%3E%0A%20%20%20%20%20%20%20%20%20%20%3Cdiv%20class%3D%22col-md-9%20offset-md-3%22%3E%0A%20%20%20%20%20%20%20%20%20%20%20%20%3Ch2%3EA%20First%20CUDA%20C%20Program%3C%2Fh2%3E%0A%20%20%20%20%20%20%20%20%20%20%20%20In%20a%20recent%20post%2C%20I%20illustrated%20Six%20Ways%20to%20SAXPY%2C%20which%20includes%20a%20CUDA%20C%20version.%20SAXPY%20stands%20for%20%E2%80%9CSingle-precision%20A%2AX%20Plus%20Y%E2%80%9D%2C%20and%20is%20a%20good%20%E2%80%9Chello%20world%E2%80%9D%20example%20for%20parallel%20computation.%20In%20this%20post%20I%20will%20dissect%20a%20more%20complete%20version%20of%20the%20CUDA%20C%20SAXPY%2C%20explaining%20in%20detail%20what%20is%20done%20and%20why.%20The%20complete%20SAXPY%20code%20is%3A%0A%20%20%20%20%20%20%20%20%20%20%3C%2Fdiv%3E%0A%20%20%20%20%20%20%20%20%3C%2Fdiv%3E%0A%20%20%20%20%20%20%3C%2Fdiv%3E%0A%20%20%20%20%3C%2Fbody%3E');

COMMIT;


-- -----------------------------------------------------
-- Data for table `programing`.`users`
-- -----------------------------------------------------
START TRANSACTION;
USE `programing`;
INSERT INTO `programing`.`users` (`username`, `password`) VALUES ('admin', '74913f5cd5f61ec0bcfdb775414c2fb3d161b620');

COMMIT;






