<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="styles.css">
    <title>Hello, world!</title>
  </head>
  <div id="main">
    <body>
  <nav class="navbar navbar-light bg-light">
    <a class="navbar-brand" href="#">
      <img src="https://upload.wikimedia.org/wikipedia/en/thumb/1/1c/OpenCL_Logo.svg/1024px-OpenCL_Logo.svg.png" width="50" height="50" class="d-inline-block align-top" alt="" >
        How to code in OPENCL paralel
    </a>
    <select class="custom-select" onchange='izbral_temo()' id="selection">
      <option value="opencl">learn OpenCL</option>
      <option value="cuda">learn CUDA</option>
    </select>

  </nav>


  <div class="container">
    <div class="row">
      <div class="col-md-9 offset-md-3">
        <h1>Getting started with OpenCL and GPU Computing</h1>
OpenCL (Open Computing Language) is a new framework for writing programs that execute in parallel on different compute devices (such as CPUs and GPUs) from different vendors (AMD, Intel, ATI, Nvidia etc.). The framework defines a language to write “kernels” in. These kernels are the functions which are to run on the different compute devices. In this post I explain how to get started with OpenCL and how to make a small OpenCL program that will compute the sum of two lists in parallel.
      </div>
    </div>
    <div class="row top-buffer">
      <div class="col-md-9 offset-md-3">
        <h2>Your first OpenCL program – Vector addition</h2>
To demonstrate OpenCL I explain how to perform the simple task of vector addition. Suppose we have two lists of numbers, A and B, of equal size. The task of vector addition is to add the elements of A with the elements of B and put the result in the element of a new list called C of the same size. The figure below explains the operation.
      </div>
    </div>
    <div id="open_file">first_opencl.cpp</div>
  </div>
</body>
  </div>
  <div class="container">
  <div class="row top-buffer">
    <div class="col-md-9 offset-md-3" id="file_prog" >
    </div>
  </div>
  </div>
  <script>

  window.addEventListener("load", function(){
    var sel="cuda";
      jQuery.ajax({
        type: "POST",
        url: './executables.php',
        data: {functionname: 'getText', type: [sel]},
        success:function(data) {
        document.getElementById("main").innerHTML=data;
        get_file("first.cpp");
        }
    });
  });



  function izbral_temo(){
    var sel=document.getElementById("selection").value;
      jQuery.ajax({
        type: "POST",
        url: './executables.php',
        data: {functionname: 'getText', type: [sel]},
        success:function(data) {
        document.getElementById("main").innerHTML="";
        document.getElementById("main").innerHTML=data;

        if(sel=="cuda"){
          get_file("first.cpp");
        }else if(sel=="opencl"){
          get_file("first_opencl.cpp");
        }
        }
    });
  }


  function get_file(data){

  jQuery.ajax({
    type: "POST",
    url: './executables.php',
    data: {functionname: 'output_prog', type: [data]},
    success:function(data) {
    document.getElementById("file_prog").innerHTML=data;
    document.getElementById("file_prog").style.fontSize="small";
    }
});
}

  </script>
</html>
