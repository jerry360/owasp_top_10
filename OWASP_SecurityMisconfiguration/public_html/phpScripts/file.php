<?php
$target_dir = "../uploads/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["fileToUpload"]["name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
}
if (file_exists($target_dir . basename($_FILES["fileToUpload"]["tmp_name"]))) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}
if ($_FILES["fileToUpload"]["size"] > 500000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_dir . basename($_FILES["fileToUpload"]["name"]))) {
        echo "The file ". basename($_FILES["fileToUpload"]["name"]). " has been uploaded. ";
        echo "You can now use the link provided to share the file";
        echo "<p><a href=http://192.168.210.117:8080/uploads/".basename($_FILES["fileToUpload"]["name"]).">http://192.168.210.117:8080/uploads/".basename($_FILES["fileToUpload"]["name"])."</a></p>";
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}
?>
