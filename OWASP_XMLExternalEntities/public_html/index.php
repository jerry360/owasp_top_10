<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>FILE SHARE</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <link href="css/login.css" rel="stylesheet">
  </head>

  <body>
    <div class="wrapper">
      <form class="form-signin" method="POST" action="phpScripts/preveri.php">
        <h2 class="form-signin-heading text-center">CLOUD FILE SHARE <img src="http://www.clker.com/cliparts/e/N/j/N/U/Q/thunder-cloud-s-cutie-mark-md.png" width="60" height="60"></h2>
        </br>
        <label for="inputEmail">Enter your username</label>
        <input type="text" class="form-control" name="user"  id="inputEmail" placeholder="Username" required="" autofocus="" />
        </br>
        <label for="inputPassword">Provide a password</label>
        <input type="password" class="form-control" name="pass" placeholder="Password" id="inputPassword" required=""/>
        </br>
        <button class="btn btn-lg btn-primary btn-block" type="submit" name="prijava">Login</button>
          <?php
          if(isset($_COOKIE["pazi"])) {
                 echo '<label>'.$_COOKIE['pazi'].'</label>';
          }
            ?>
      </form>
    </div>
    <div class="wrapper">
      <form class="form-signin" method="post" enctype="multipart/form-data" id="data">
        <label for="fileToUpload" id="stat">Upload your XML file key</label>
          <input class="form-control" type="file" name="fileToUpload" id="fileToUpload" onchange="send()"/>
          </br>
          <button class="btn btn-lg btn-primary btn-block"  id="dataButton">Login using file</button>
      </form>
    </div>
  </body>

<script>



function send(){
  var file=document.getElementById("fileToUpload");
  var txt = "";
if ('files' in file) {
  if (file.files.length == 0) {
    txt = "To use this option the file must not be empty.";
  } else {
    if (file.files.length == 1) {
      var file = file.files[0];
      if ('name' in file) {
        txt += "name: " + file.name + "<br>";
      }
      if ('size' in file) {
        txt += "size: " + file.size + " bytes <br>";
      }
    }else{


      txt="select ONE file!!"
    }
  }
}
document.getElementById ("stat").innerHTML = txt;

}


$("form#data").submit(function(e) {
    e.preventDefault();
    var formData = new FormData(this);

    console.log(formData);
    jQuery.ajax({
      type: "POST",
      url: './phpScripts/loader.php',
      data: formData,
      success: function (data) {
        document.getElementById ("stat").innerHTML = data;

        //document.getElementById ("bod").style.backgroundColor = data;
        },
      cache: false,
      contentType: false,
      processData: false
    });
});


</script>
</html>
