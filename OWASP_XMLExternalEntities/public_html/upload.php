<?php session_start();
require 'phpScripts/checkUser.php';
if(isset($_SESSION["idUser"]) && isset($_SESSION["username"])) {
    if(checkHim($_SESSION["idUser"], $_SESSION["username"])==false) {
        setcookie("pazi", "vsak poskus vdora bo prijavljen!");
        echo("<script>location.href = '../index.php';</script>");
        session_destroy();
    }else{
        echo "status: wellcome ".$_SESSION["username"];
    }
}else{
    setcookie("pazi", "vsak poskus vdora bo prijavljen!");
    echo("<script>location.href = '../index.php';</script>");
    session_destroy();
}

?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>FILE SHARE</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

  </head>

  <body>
<nav class="navbar navbar-light bg-light">
  <a class="navbar-brand" href="#">
    <img src="https://upload.wikimedia.org/wikipedia/en/thumb/1/1c/OpenCL_Logo.svg/1024px-OpenCL_Logo.svg.png" width="50" height="50" class="d-inline-block align-top" alt="" >
      How to code in OPENCL paralel
  </a>

</nav>


<div class="container">
  <div class="row">
    <div class="col-md-9 offset-md-3">
      <h1>How a Kernel Executes on an OpenCL Device</h1>
      A kernel is defined on the host. The host program issues a command that
submits the kernel for execution on an OpenCL device. When this command is issued by the host, the OpenCL runtime system creates an integer index space. An instance of the kernel executes for each point in this
index space. We call each instance of an executing kernel a work-item,
which is identified by its coordinates in the index space. These coordinates are the global ID for the work-item.
The command that submits a kernel for execution, therefore, creates a
collection of work-items, each of which uses the same sequence of instructions defined by a single kernel. While the sequence of instructions is the
same, the behavior of each work-item can vary because of branch statements within the code or data selected through the global ID.
ptg
14 Chapter 1: An Introduction to OpenCL
Work-items are organized into work-groups. The work-groups provide a
more coarse-grained decomposition of the index space and exactly span
the global index space. In other words, work-groups are the same size in
corresponding dimensions, and this size evenly divides the global size
in each dimension. Work-groups are assigned a unique ID with the same
dimensionality as the index space used for the work-items. Work-items are
assigned a unique local ID within a work-group so that a single work-item
can be uniquely identified by its global ID or by a combination of its local
ID and work-group ID.
</div>
  </div>
</br>
</br>
  <div class="row top-buffer">
    <div class="col-md-9 offset-md-3">
      <h2>Context</h2>
      The computational work of an OpenCL application takes place on the
OpenCL devices. The host, however, plays a very important role in the
OpenCL application. It is on the host where the kernels are defined. The
host establishes the context for the kernels. The host defines the NDRange
and the queues that control the details of how and when the kernels
execute. All of these important functions are contained in the APIs within
OpenCL’s definition.
The first task for the host is to define the context for the OpenCL application. As the name implies, the context defines the environment within
which the kernels are defined and execute. To be more precise, we define
the context in terms of the following resources:
• Devices: the collection of OpenCL devices to be used by the host
• Kernels: the OpenCL functions that run on OpenCL devices
• Program objects: the program source code and executables that
implement the kernels
• Memory objects: a set of objects in memory that are visible to
OpenCL devices and contain values that can be operated on by
instances of a kernel
The context is created and manipulated by the host using functions from
the OpenCL API. For example, consider the heterogeneous platform from
Figure 1.3. This system has two multicore CPUs and a GPU. The host
program is running on one of the CPUs. The host program will query the
system to discover these resources and then decide which devices to use
in the OpenCL application. Depending on the problem and the kernels to
be run, the host may choose the GPU, the other CPU, other cores on the
same CPU, or any combination of these. Once made, this choice defines
the OpenCL devices within the current context.
Also included in the context are one or more program objects that contain the code for the kernels. The choice of the name program object is a bit
confusing. It is better to think of these as a dynamic library from which
ptg
18 Chapter 1: An Introduction to OpenCL
the functions used by the kernels are pulled. The program object is built
at runtime within the host program. This might seem strange to programmers from outside the graphics community. Consider for a moment the
challenge faced by an OpenCL programmer. He or she writes the OpenCL
application and passes it to the end user, but that user could choose to run
the application anywhere. The application programmer has no control
over which GPUs or CPUs or other chips the end user may run the application on. All the OpenCL programmer knows is that the target platform
will be conformant to the OpenCL specification.
</div>
  </div>

</div>
</body>
</html>
