<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="styles.css">
    <title>Hello, world!</title>
  </head>
  <div>
    <body>
  <nav class="navbar navbar-light bg-light">
    <a class="navbar-brand" href="#">
      <img src="https://upload.wikimedia.org/wikipedia/en/thumb/1/1c/OpenCL_Logo.svg/1024px-OpenCL_Logo.svg.png" width="50" height="50" class="d-inline-block align-top" alt="" >
        How to code in OPENCL paralel
    </a>
    <div class="input-group-prepend">
      <input type="text" class="form-control" id="vnosSearch" placeholder="search by content" aria-label="" aria-describedby="basic-addon1">
      <button class="btn btn-secondary btn-block btn-sm" type="button" onclick="searchComment()">SEARCH THE PAGE</button>
    </div>
  </nav>


  <div class="container">
    <div class="row">
      <div class="col-md-9 offset-md-3">
        <h1><div id="open_file"></div></h1>
        <h1>OpenCL Sorting Networks</h1>
        This sample implements bitonic sort algorithm for batches of short arrays
      </div>
    </div>
  </div>
  </div>
  <div class="container">
  <div class="row top-buffer">
    <div class="col-md-9 offset-md-3" id="file_prog" >
    </div>
  </div>
  </div>

  <div class="container">
    <div class="row top-buffer">
      <div class="col-md-9 offset-md-3" id="postComment" >
        <form class="form-signin" method="POST" id="data">
        </br>
        <label for="inputUsername">anonymous username</label>
        <input type="text" class="form-control" name="Usename"  id="inputUsername" placeholder="This will be displayed besides your comment!" required="" autofocus="" />
        </br>
          <label for="inputComment">Add your comment</label>
          <input type="text" class="form-control" name="comment"  id="inputComment" placeholder="Write something nice!" required="" autofocus="" />
          </br>
          <button class="btn btn-lg btn-primary btn-block" type="submit" name="postIT">Add a comment</button>
        </form>
      </div>
    </div>
  </div>


  <div class="container">
  <div class="row top-buffer">
    <div class="col-md-9 offset-md-3">
      <table class="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Username</th>
            <th scope="col">Comment</th>
            <th scope="col">  </th>
          </tr>
        </thead>
        <tbody id="comments">
        </tbody>
      </table>
    </div>
  </div>
  </div>




  <script>

  //patrik.rek@um.si
function searchComment(){
  var Sstring=document.getElementById("vnosSearch").value;
  if(Sstring!=""){
    location.href = "../search.php?what="+Sstring;
  }
}


  window.addEventListener("load", function(){
        get_file("first.cpp");
        getComments();
  });

function getComments(){
  jQuery.ajax({
    type: "POST",
    url: './executables.php',
    data: {functionname: 'getComments'},
    success:function(data) {
    document.getElementById("comments").innerHTML="";
    document.getElementById("comments").innerHTML=data;
  }
});
}



  function get_file(data){

  jQuery.ajax({
    type: "POST",
    url: './executables.php',
    data: {functionname: 'output_prog', type: [data]},
    success:function(data) {
    document.getElementById("file_prog").innerHTML=data;
    document.getElementById("file_prog").style.fontSize="small";
    }
});
}

$("form#data").submit(function(e) {
    e.preventDefault();
    //alert(document.getElementById("inputComment").value);
    jQuery.ajax({
      type: "POST",
      url: './executables.php',
      data: {functionname: 'postAcomment', type: [document.getElementById("inputComment").value,document.getElementById("inputUsername").value]},
      success:function(data) {
        alert(data);
        getComments();
      }
  });
});




  </script>
  </body>
</html>
