<!doctype html>
<?php
$file = './SCRIPTS/first.cpp';
$searchfor = $_GET["what"];
$contents = file_get_contents($file);
$pattern = preg_quote($searchfor, '/');
$pattern = "/^.*$pattern.*\$/m";
 ?>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="styles.css">
    <title>Hello, world!</title>
  </head>
  <body>
    <nav class="navbar navbar-light bg-light">
      <a class="navbar-brand" href="./index.php">
        <img src="https://upload.wikimedia.org/wikipedia/en/thumb/1/1c/OpenCL_Logo.svg/1024px-OpenCL_Logo.svg.png" width="50" height="50" class="d-inline-block align-top" alt="" >
          BACK
      </a>
    </nav>
    <h2><div id="output"><?php
    if(preg_match_all($pattern, $contents, $matches)){
       echo "Found matches:\n";
       echo implode("\n", $matches[0]);
       $output = shell_exec("./SCRIPTS/catinator3000 ./SCRIPTS/first.cpp");
       echo "</h2>".nl2br($output);
    }
    else{
       echo "No matches for search string: ".$_GET["what"];
    }


    ?></div></h2>

</body>
</html>
