Standard utilities and systems includesssssssssss<br />
#include <oclUtils.h><br />
#include <shrQATest.h><br />
<br />
#include "oclSortingNetworkssssss_common.haaaaaaaa"<br />
<br />
////////////////////////////////////////////////////////////////////////////////<br />
//Test driver<br />
////////////////////////////////////////////////////////////////////////////////<br />
int main(int argc, const char **argv){<br />
    cl_platform_id cpPlatform;<br />
    cl_device_id cdDevice;<br />
    cl_context cxGPUContext;<br />
    cl_command_queue cqCommandQueue;<br />
    cl_mem d_InputKey, d_InputVal, d_OutputKey, d_OutputVal;<br />
<br />
    cl_int ciErrNum;<br />
    uint *h_InputKey, *h_InputVal, *h_OutputKeyGPU, *h_OutputValGPU;<br />
<br />
    const uint dir = 1;<br />
    const uint N = 1048576;<br />
    const uint numValues = 65536;<br />
<br />
    shrQAStart(argc, (char **)argv);<br />
<br />
    // set logfile name and start logs<br />
    shrSetLogFileName ("oclSortingNetworks.txt");<br />
    shrLog("%s Starting...\n\n", argv[0]);<br />
<br />
    shrLog("Initializing data...\n");<br />
        h_InputKey      = (uint *)malloc(N * sizeof(uint));<br />
        h_InputVal      = (uint *)malloc(N * sizeof(uint));<br />
        h_OutputKeyGPU  = (uint *)malloc(N * sizeof(uint));<br />
        h_OutputValGPU  = (uint *)malloc(N * sizeof(uint));<br />
        srand(2009);<br />
        for(uint i = 0; i < N; i++)<br />
            h_InputKey[i] = rand() % numValues;<br />
        fillValues(h_InputVal, N);<br />
<br />
    shrLog("Initializing OpenCL...\n");<br />
        //Get the NVIDIA platform<br />
        ciErrNum = oclGetPlatformID(&cpPlatform);<br />
        oclCheckError(ciErrNum, CL_SUCCESS);<br />
<br />
        //Get the devices<br />
        ciErrNum = clGetDeviceIDs(cpPlatform, CL_DEVICE_TYPE_GPU, 1, &cdDevice, NULL);<br />
        oclCheckError(ciErrNum, CL_SUCCESS);<br />
<br />
        //Create the context<br />
        cxGPUContext = clCreateContext(0, 1, &cdDevice, NULL, NULL, &ciErrNum);<br />
        oclCheckError(ciErrNum, CL_SUCCESS);<br />
<br />
        //Create a command-queue<br />
        cqCommandQueue = clCreateCommandQueue(cxGPUContext, cdDevice, CL_QUEUE_PROFILING_ENABLE, &ciErrNum);<br />
        oclCheckError(ciErrNum, CL_SUCCESS);<br />
<br />
    shrLog("Initializing OpenCL bitonic sorter...\n");<br />
        initBitonicSort(cxGPUContext, cqCommandQueue, argv);<br />
<br />
    shrLog("Creating OpenCL memory objects...\n\n");<br />
        d_InputKey = clCreateBuffer(cxGPUContext, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, N * sizeof(cl_uint), h_InputKey, &ciErrNum);<br />
        oclCheckError(ciErrNum, CL_SUCCESS);<br />
        d_InputVal = clCreateBuffer(cxGPUContext, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, N * sizeof(cl_uint), h_InputVal, &ciErrNum);<br />
        oclCheckError(ciErrNum, CL_SUCCESS);<br />
        d_OutputKey = clCreateBuffer(cxGPUContext, CL_MEM_READ_WRITE, N * sizeof(cl_uint), NULL, &ciErrNum);<br />
        oclCheckError(ciErrNum, CL_SUCCESS);<br />
        d_OutputVal = clCreateBuffer(cxGPUContext, CL_MEM_READ_WRITE, N * sizeof(cl_uint), NULL, &ciErrNum);<br />
        oclCheckError(ciErrNum, CL_SUCCESS);<br />
<br />
    //Temp storage for key array validation routine<br />
    uint *srcHist = (uint *)malloc(numValues * sizeof(uint));<br />
    uint *resHist = (uint *)malloc(numValues * sizeof(uint));<br />
<br />
#ifdef GPU_PROFILING<br />
    cl_event startTime, endTime;<br />
#endif<br />
<br />
    int globalFlag = 1;// init pass/fail flag to pass<br />
    for(uint arrayLength = 64; arrayLength <= N; arrayLength *= 2){<br />
        shrLog("Test array length %u (%u arrays in the batch)...\n", arrayLength, N / arrayLength);<br />
<br />
#ifdef GPU_PROFILING<br />
            clFinish(cqCommandQueue);<br />
            ciErrNum = clEnqueueMarker(cqCommandQueue, &startTime);<br />
            oclCheckError(ciErrNum, CL_SUCCESS);<br />
            shrDeltaT(0);<br />
#endif<br />
<br />
            size_t szWorkgroup = bitonicSort(<br />
                NULL,<br />
                d_OutputKey,<br />
                d_OutputVal,<br />
                d_InputKey,<br />
                d_InputVal,<br />
                N / arrayLength,<br />
                arrayLength,<br />
                dir<br />
            );<br />
            oclCheckError(szWorkgroup > 0, true);<br />
<br />
#ifdef GPU_PROFILING<br />
            if (arrayLength == N)<br />
            {<br />
                ciErrNum = clEnqueueMarker(cqCommandQueue, &endTime);<br />
                oclCheckError(ciErrNum, CL_SUCCESS);<br />
                clFinish(cqCommandQueue);<br />
                double timerValue = shrDeltaT(0);<br />
                shrLogEx(LOGBOTH | MASTER, 0, "oclSortingNetworks-bitonic, Throughput = %.4f MElements/s, Time = %.5f s, Size = %u elements, NumDevsUsed = %u, Workgroup = %u\n",<br />
                       (1.0e-6 * (double)arrayLength/timerValue), timerValue, arrayLength, 1, szWorkgroup);<br />
<br />
                cl_ulong startTimeVal = 0, endTimeVal = 0;<br />
                ciErrNum = clGetEventProfilingInfo(<br />
                    startTime,<br />
                    CL_PROFILING_COMMAND_END,<br />
                    sizeof(cl_ulong),<br />
                    &startTimeVal,<br />
                    NULL<br />
                );<br />
<br />
                ciErrNum = clGetEventProfilingInfo(<br />
                    endTime,<br />
                    CL_PROFILING_COMMAND_END,<br />
                    sizeof(cl_ulong),<br />
                    &endTimeVal,<br />
                    NULL<br />
                );<br />
<br />
                shrLog("OpenCL time: %.5f s\n", 1.0e-9 * (double)(endTimeVal - startTimeVal));<br />
            }<br />
#endif<br />
<br />
        //Reading back results from device to host<br />
        ciErrNum = clEnqueueReadBuffer(cqCommandQueue, d_OutputKey, CL_TRUE, 0, N * sizeof(cl_uint), h_OutputKeyGPU, 0, NULL, NULL);<br />
        oclCheckError(ciErrNum, CL_SUCCESS);<br />
        ciErrNum = clEnqueueReadBuffer(cqCommandQueue, d_OutputVal, CL_TRUE, 0, N * sizeof(cl_uint), h_OutputValGPU, 0, NULL, NULL);<br />
        oclCheckError(ciErrNum, CL_SUCCESS);<br />
<br />
        //Check if keys array is not corrupted and properly ordered<br />
        int keysFlag = validateSortedKeys(h_OutputKeyGPU, h_InputKey, N / arrayLength, arrayLength, numValues, dir, srcHist, resHist);<br />
<br />
        //Check if values array is not corrupted<br />
        int valuesFlag = validateSortedValues(h_OutputKeyGPU, h_OutputValGPU, h_InputKey, N / arrayLength, arrayLength);<br />
<br />
        // accumulate any error or failure<br />
        globalFlag = globalFlag && keysFlag && valuesFlag;<br />
    }<br />
<br />
    // Start Cleanup<br />
    shrLog("Shutting down...\n");<br />
        //Discard temp storage for key validation routine<br />
        free(srcHist);<br />
        free(resHist);<br />
<br />
        //Release kernels and program<br />
        closeBitonicSort();<br />
<br />
        //Release other OpenCL Objects<br />
        ciErrNum  = clReleaseMemObject(d_OutputVal);<br />
        ciErrNum |= clReleaseMemObject(d_OutputKey);<br />
        ciErrNum |= clReleaseMemObject(d_InputVal);<br />
        ciErrNum |= clReleaseMemObject(d_InputKey);<br />
        ciErrNum |= clReleaseCommandQueue(cqCommandQueue);<br />
        ciErrNum |= clReleaseContext(cxGPUContext);<br />
        oclCheckError(ciErrNum, CL_SUCCESS);<br />
<br />
        //Release host buffers<br />
        free(h_OutputValGPU);<br />
        free(h_OutputKeyGPU);<br />
        free(h_InputVal);<br />
        free(h_InputKey);<br />
<br />
    // finish<br />
    // pass or fail (cumulative... all tests in the loop)<br />
    shrQAFinishExit(argc, (const char **)argv, globalFlag ? QA_PASSED : QA_FAILED);<br />
<br />
        //Finish<br />
        shrEXIT(argc, argv);<br />
}<br />
