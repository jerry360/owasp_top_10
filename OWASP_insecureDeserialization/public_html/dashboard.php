<?php session_start();
require 'phpScripts/checkUser.php';
if(isset($_SESSION["idUser"]) && isset($_SESSION["username"])) {
    if(checkHimAdmin($_SESSION["idUser"], $_SESSION["username"])==false) {
        setcookie("pazi", "vsak poskus vdora bo prijavljen!");
        echo("<script>location.href = '../index.php';</script>");
        session_destroy();
    }else{
        echo "status: wellcome ".$_SESSION["username"];
    }
}else{
    setcookie("pazi", "vsak poskus vdora bo prijavljen!");
    echo("<script>location.href = '../index.php';</script>");
    session_destroy();
}

?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <title>Hello, world!</title>
  </head>

    <body>
    <nav class="navbar navbar-light bg-light">
        <a href="javascript:saveIt();"><h1><img src="https://image.flaticon.com/icons/png/512/84/84380.png" width="50" height="50" class="d-inline-block align-top" alt="" /> SAVE</h1></a><h3>Edit today's lession</h3>
    </nav>


    <div class="container">
      <div class="row">
        <div class="col-md-9 offset-md-3">
          <textarea rows="30" cols="120" id="file_prog"></textarea>
        </div>
      </div>
    </div>

  </body>

<script>

window.addEventListener("load", function(){
      get_file("first.cpp");
});




function get_file(data){

jQuery.ajax({
  type: "POST",
  url: './executables.php',
  data: {functionname: 'output_prog', type: [data]},
  success:function(data) {
  document.getElementById("file_prog").value=data;
  document.getElementById("file_prog").style.fontSize="small";
  }
});
}


function saveIt(){
  var data=document.getElementById("file_prog").value;
  jQuery.ajax({
    type: "POST",
    url: './executables.php',
    data: {functionname: 'saveit', type: [data]},
    success:function(data) {
    alert(data);
    location.href = './upload.php';
    }
  });
}





</script>

</html>
