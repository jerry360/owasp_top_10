<?php


$output="";

if(isset($_POST['submit'])) {
    if(!empty($_POST['answer_list'])) {
        $checked_count = count($_POST['answer_list']);
        if($checked_count!=2) {
            $output= "one answer per question please!";
        }else{
            $first=false;
            $second=false;
            foreach($_POST['answer_list'] as $selected) {
                if($selected=="Kernals") {
                    $first=true;
                }
                if($selected=="global memory, read-only memory,local memory,per-element private memory") {
                    $second=true;
                }
            }

            if($first && $second) {
                $output= "congrats all your answers are correct";
                setcookie("SCORE", "2", time() + (86400 * 30), "/");
            }
            if($first && !($second)|| !($first) && $second) {
                $output= "at least one of them was correct hahaha";
                setcookie("SCORE", "1", time() + (86400 * 30), "/");
            }
            if(!($first) && !($second)) {
                $output= "sorry you will have to study harder";
                setcookie("SCORE", "0", time() + (86400 * 30), "/");
            }
        }
    }else{
        $output= "you must know at least one answer right!?";
    }
}



?>
<?php session_start();
require './checkUser.php';
if(isset($_SESSION["idUser"]) && isset($_SESSION["username"])) {
    if(checkHim($_SESSION["idUser"], $_SESSION["username"])==false) {
        setcookie("pazi", "vsak poskus vdora bo prijavljen!");
        echo("<script>location.href = '../index.php';</script>");
        session_destroy();
    }else{
        echo "status: wellcome ".$_SESSION["username"];
    }
}else{
    setcookie("pazi", "vsak poskus vdora bo prijavljen!");
    echo("<script>location.href = '../index.php';</script>");
    session_destroy();
}


?>
<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <title>Hello, world!</title>
  <link href="../css/ans.css" rel="stylesheet">
</head>


  <body class="bg-image">
    <div class="main">
      <h1>
        <?php
        echo $output;
        ?>
      </h1>
      <button class="btn btn-lg btn-primary btn-block"  onclick="location.href='../quiz.php';">RETURN TO QUIZ</button>
      <br/>
      <button class="btn btn-success"  onclick="postIt()">Share on a scoreboard</button>
      <div class="hisAns">
        <h6>Your answers were:</h6>
        <?php
        if(isset($_POST['submit'])) {
            if(!empty($_POST['answer_list'])) {
                foreach($_POST['answer_list'] as $selected){
                    echo $selected."<br/>";
                }
            }
        }
        ?>
      </div>
    </div>
    <script>

    function getCookie(cname) {
      var name = cname + "=";
      var decodedCookie = decodeURIComponent(document.cookie);
      var ca = decodedCookie.split(';');
      for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
          c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
          return c.substring(name.length, c.length);
        }
      }
      return "";
    }


    function postIt(){
      var data =getCookie("SCORE");
      jQuery.ajax({
        type: "POST",
        url: '../executables.php',
        data: {functionname: 'postMyScore', type: [data]},
        success:function(data) {
          alert(data);
        }
      });
    }
    </script>
  </body>
</html>
