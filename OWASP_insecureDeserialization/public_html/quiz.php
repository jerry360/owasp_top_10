<?php session_start();
require 'phpScripts/checkUser.php';
if(isset($_SESSION["idUser"]) && isset($_SESSION["username"])) {
    if(checkHim($_SESSION["idUser"], $_SESSION["username"])==false) {
        setcookie("pazi", "vsak poskus vdora bo prijavljen!");
        echo("<script>location.href = '../index.php';</script>");
        session_destroy();
    }else{
        echo "status: wellcome ".$_SESSION["username"];
    }
}else{
    setcookie("pazi", "vsak poskus vdora bo prijavljen!");
    echo("<script>location.href = '../index.php';</script>");
    session_destroy();
}

?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <link href="css/quiz.css" rel="stylesheet">
  </head>
  <body class="bg-image">
    <nav class="navbar navbar-light bg-light">
      <a class="navbar-brand" href="./upload.php?type=user">
        <h2>
          <img src="https://image.flaticon.com/icons/png/512/16/16116.png" width="40" height="40" class="d-inline-block align-top" alt="" >  BACK
        </h2>
      </a>
      <a class="navbar-brand" href="./scoreboard.php">
        <h1>
          SCORE BOARD <img src="https://cdn3.iconfinder.com/data/icons/soccer-14/33/scoreboard-512.png" width="60" height="60" class="d-inline-block align-top" alt="" >
        </h1>
      </a>
    </nav>
    <div class="main">
      <small>select one answer per question*</small>
      <form name="answers" method="post" action="./phpScripts/checkAns.php">
        <h4>1.  Functions executed on an OpenCL device are called:</h4>
          <input type="checkbox" name="answer_list[]" value="Kernals"/>
          <label for="ans1.1">Kernals</label>
        </br>
          <input type="checkbox" name="answer_list[]" value="Cores"/>
          <label for="ans1.2">Cores</label>
        </br>
          <input type="checkbox" name="answer_list[]" value="Models"/>
          <label for="ans1.3">Models</label>
        </br>
        </br>
        <h4>2.  OpenCL defines a four-level memory hierarchy for the compute device:</h4>
        </br>
          <input type="checkbox" name="answer_list[]" value="global memory, read-only memory,local memory,per-element private memory"/>
          <label for="ans2.1">global memory, read-only memory,local memory,per-element private memory</label>
        </br>
          <input type="checkbox" name="answer_list[]" value="main memory, secondary memory, external memory, internal memory"/>
          <label for="ans2.2">main memory, secondary memory, external memory, internal memory</label>
        </br>
          <input type="checkbox" name="answer_list[]" value="fast memory, slow memory, efficient memory, inefficient memory"/>
          <label for="ans2.3">fast memory, slow memory, efficient memory, inefficient memory</label>
        <button class="btn btn-lg btn-primary btn-block" type="submit" name="submit">SUBMIT ANSWERS</button>
      </from>
    </div>
  </body>
</html>
