<?php session_start();
require 'phpScripts/checkUser.php';
if(isset($_SESSION["idUser"]) && isset($_SESSION["username"])) {
    if(checkHim($_SESSION["idUser"], $_SESSION["username"])==false) {
        setcookie("pazi", "vsak poskus vdora bo prijavljen!");
        echo("<script>location.href = '../index.php';</script>");
        session_destroy();
    }else{
        echo "status: wellcome ".$_SESSION["username"];
    }
}else{
    setcookie("pazi", "vsak poskus vdora bo prijavljen!");
    echo("<script>location.href = '../index.php';</script>");
    session_destroy();
}

?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <link href="css/quiz.css" rel="stylesheet">
  </head>

  <body class="bg-image">
    <div class="main">
      <table class='table'>
        <tbody>

        <?php
        $fn = fopen("scores.txt", "r");

        while(! feof($fn))  {
            $result = fgets($fn);
            echo "<tr>".$result."</tr>";
        }

        fclose($fn);
        ?>

      </tbody>
     </table>
    </div>
  </body>
</html>
